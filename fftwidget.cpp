#include "fftwidget.h"

FFTWidget::FFTWidget(QWidget *parent) :
    QwtPlot(parent)
{
    setAutoReplot(false);
    InitGradient();

    plotLayout()->setAlignCanvasToScales(true);
    setAxisTitle(QwtPlot::xBottom, "Frequency, Hz");
    setAxisScale(QwtPlot::xBottom, 0.0, 400.0);
    setAxisTitle(QwtPlot::yLeft, "Magnitude, dB");
    setAxisScale(QwtPlot::yLeft, -80.0, 0.0);

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::green, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    InitData();
    m_pCurveMagnitude = new QwtPlotCurve();
    m_pCurveMagnitude->setStyle(QwtPlotCurve::Lines);
    m_pCurveMagnitude->setPen(QPen(Qt::red, 2));
    m_pCurveMagnitude->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    m_pCurveMagnitude->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    m_pCurveMagnitude->setRawSamples(m_MagnitudePosition.data(), m_Magnitude.data(), m_MagnitudePosition.size());
    m_pCurveMagnitude->attach(this);
}

FFTWidget::~FFTWidget()
{

}

void FFTWidget::receiveMagnitide(Magnitude &magnitude)
{
    for (size_t i = 0; i < magnitude.size(); i++)
        m_Magnitude[i] = magnitude[i];
    replot();
}

void FFTWidget::setVertical(int interval)
{
    verticalInterval.setMinValue(-82.0);
    verticalInterval.setMaxValue(interval);
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());
    replot();
}

void FFTWidget::InitGradient()
{
    QPalette pal = canvas()->palette();
    QLinearGradient gradient(0.0, 0.0, 1.0, 1.0);
    gradient.setCoordinateMode(QGradient::StretchToDeviceMode);
    gradient.setColorAt(0.0, QColor(0, 49, 110));
    gradient.setColorAt(1.0, QColor(0, 87, 174));

    pal.setBrush(QPalette::Window, QBrush(gradient));
    canvas()->setPalette(pal);
}

void FFTWidget::InitData()
{
    for (uint i = 0; i < SPECTRAL_BANDS; i++) {
        double freq = (gdouble)((AUDIOFREQ/2)*i + AUDIOFREQ/4)/SPECTRAL_BANDS;
        m_MagnitudePosition.push_back(freq);
        m_Magnitude.push_back(-80.0);
    }
}
