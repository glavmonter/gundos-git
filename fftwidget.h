#ifndef FFTWIDGET_H
#define FFTWIDGET_H

#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include "gstengine.h"

class FFTWidget : public QwtPlot
{
    Q_OBJECT

public:
    FFTWidget(QWidget *parent = NULL);
    ~FFTWidget();

public slots:
    void receiveMagnitide(Magnitude &magnitude);
    void setVertical(int interval);

private:
    void InitGradient();
    void InitData();
    Magnitude m_Magnitude;
    Magnitude m_MagnitudePosition;

    QwtInterval verticalInterval;
    QwtPlotCurve *m_pCurveMagnitude;
};

#endif // FFTWIDGET_H
