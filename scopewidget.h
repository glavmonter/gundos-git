#ifndef SCOPEWIDGET_H
#define SCOPEWIDGET_H

#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include "gstengine.h"

typedef std::vector<double> ScopeDouble;

class ScopeWidget : public QwtPlot
{
    Q_OBJECT

public:
    ScopeWidget(QWidget *parent = NULL);
    ~ScopeWidget();

public slots:
    void receiveScope(Scope & scope, quint64 duration);
    void setVertical(int interval);

private:
    void InitGradient();
    void InitData();
    std::vector<double> m_Scope;
    std::vector<double> m_ScopePosition;

    QwtInterval verticalInterval;
    QwtPlotCurve *m_pCurveScope;
};

#endif // SCOPEWIDGET_H
