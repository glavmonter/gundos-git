#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <gst/gst.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qRegisterMetaType<Scope>("Scope&");
    qRegisterMetaType<Magnitude>("Magnitude&");

    ui->setupUi(this);
    m_pTimer = NULL;

    m_pEngine = new GstEngine(this);
    m_pEngine->InitEngine();

    connect(m_pEngine, SIGNAL(Rms(double)), this, SLOT(updateRMS(double)));
    connect(ui->cmbBandwidth, SIGNAL(currentIndexChanged(int)), m_pEngine, SLOT(setBandwidth(int)));
    connect(ui->sliderInGain, SIGNAL(valueChanged(int)), m_pEngine, SLOT(setInGain(int)));
    connect(ui->sliderOutGain, SIGNAL(valueChanged(int)), m_pEngine, SLOT(setOutGain(int)));
    connect(ui->sliderOutFreq, SIGNAL(valueChanged(int)), m_pEngine, SLOT(setFreq(int)));
    connect(ui->sliderInterval, SIGNAL(valueChanged(int)), ui->frameScope, SLOT(setVertical(int)));
    connect(ui->sliderIntervalM, SIGNAL(valueChanged(int)), ui->frameMagnitude, SLOT(setVertical(int)));

    connect(m_pEngine, SIGNAL(sendScope(Scope&,quint64)), ui->frameScope, SLOT(receiveScope(Scope&,quint64)), Qt::QueuedConnection);
    connect(m_pEngine, SIGNAL(sendMagnitude(Magnitude&)), ui->frameMagnitude, SLOT(receiveMagnitide(Magnitude&)), Qt::QueuedConnection);

    QSettings settings("MUCTR", "Gundos", this);
    ui->cmbBandwidth->setCurrentIndex(settings.value("bandwidth").toInt());
    ui->sliderInGain->setValue(settings.value("inGain").toInt());
    ui->sliderOutGain->setValue(settings.value("outGain").toInt());
    ui->sliderOutFreq->setValue(settings.value("outFreq").toInt());
    ui->sliderInterval->setValue(settings.value("verticalInterval").toInt());
    ui->sliderIntervalM->setValue(settings.value("magnInterval").toInt());

    m_pTimer = new QTimer(this);
    m_pTimer->setSingleShot(true);
    m_pTimer->setInterval(2000);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(writeSettings()));
}

MainWindow::~MainWindow()
{
    writeSettings();
    delete ui;
}

void MainWindow::updateRMS(double rms)
{
    ui->labelRms->setText("RMS: " + QString::number(rms, 'g', 4) + " dB");
}

void MainWindow::receiveScope(Scope &scope, quint64 duration)
{
    m_Scope = scope;
    m_iDuration = duration;
    qDebug() << QString("scope.size: %1, duration: %2").arg(m_Scope.size()).arg(m_iDuration);
}

void MainWindow::on_btnStart_clicked()
{
    if (m_pEngine->changeState() == true) {
        ui->btnStart->setText(tr("Stop"));
    } else {
        ui->btnStart->setText(tr("Start"));
    }
}

void MainWindow::writeSettings()
{
    qDebug() << "Write settings";
    QSettings settings("MUCTR", "Gundos", this);
    settings.setValue("bandwidth", ui->cmbBandwidth->currentIndex());
    settings.setValue("inGain", ui->sliderInGain->value());
    settings.setValue("outGain", ui->sliderOutGain->value());
    settings.setValue("outFreq", ui->sliderOutFreq->value());
    settings.setValue("verticalInterval", ui->sliderInterval->value());
    settings.setValue("magnInterval", ui->sliderIntervalM->value());
}

void MainWindow::on_sliderOutGain_valueChanged()
{
    if (m_pTimer)
        m_pTimer->start();
}

void MainWindow::on_sliderOutFreq_valueChanged()
{
    if (m_pTimer)
        m_pTimer->start();
}

void MainWindow::on_sliderInGain_valueChanged()
{
    if (m_pTimer)
        m_pTimer->start();
}

void MainWindow::on_cmbBandwidth_currentIndexChanged()
{
    if (m_pTimer)
        m_pTimer->start();
}

void MainWindow::on_sliderInterval_valueChanged()
{
    if (m_pTimer)
        m_pTimer->start();
}
