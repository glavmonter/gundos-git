#include "gstengine.h"

GstEngine::GstEngine(QObject *parent) :
    QObject(parent),
    m_dOutGain(1.0),
    m_dInGain(1.0),
    m_dFreq(20.0),
    m_dBandwidth(1.0),
    m_bIsPlaying(false),
    m_iNextElementId(0)
{
    m_scope.clear();
    m_magnitude.clear();
    m_iDuration = 0;
}

GstEngine::~GstEngine()
{
    gst_element_set_state(m_pInPipeline, GST_STATE_NULL);
    gst_element_set_state(m_pOutPipeline, GST_STATE_NULL);
}

bool GstEngine::InitEngine()
{
    GstBus *bus = NULL;

    /* Создаем устройства для вывода гундоса */
    m_pOutPipeline = gst_pipeline_new("Gundos Out");
    m_pOutBin = gst_bin_new("outputbin");
    gst_bin_add(GST_BIN(m_pOutPipeline), m_pOutBin);

    m_pOutSrc       = CreateElement("audiotestsrc",     m_pOutBin);
    m_pOutVolume    = CreateElement("volume",           m_pOutBin);
    m_pOutConvert   = CreateElement("audioconvert",     m_pOutBin);
    m_pOutSink      = CreateElement("autoaudiosink",    m_pOutBin);

    GstCaps *caps;
    caps = gst_caps_from_string(CAPS_OUTPUT);
    if (!gst_element_link_filtered(m_pOutSrc, m_pOutConvert, caps)) {
        emit Error(QString("Cannot link output pipeline"));
        gst_caps_unref(caps);
        return false;
    }
    gst_caps_unref(caps);

    if (!gst_element_link_many(m_pOutConvert, m_pOutVolume, m_pOutSink, NULL)) {
        emit Error(QString("Cannot link output pipeline"));
        return false;
    }

    g_object_set(G_OBJECT(m_pOutSrc), "wave", 0, NULL);

    bus = gst_element_get_bus(m_pOutPipeline);
    gst_bus_add_watch(bus, (GstBusFunc)on_out_message, this);
    gst_object_unref(bus);


    /* Создаем устройства для ввода и анализа гундоса */
    m_pInPipeline = gst_pipeline_new("Gundos In");
    m_pInBin = gst_bin_new("inputbin");
    gst_bin_add(GST_BIN(m_pInPipeline), m_pInBin);

    m_pInSrc                = CreateElement("autoaudiosrc",     m_pInBin);
    m_pInVolume             = CreateElement("volume",           m_pInBin);
    m_pInQueueChebBand      = CreateElement("queue",            m_pInBin);
    m_pInChebBand           = CreateElement("audiochebband",    m_pInBin);
    m_pInTee                = CreateElement("tee",              m_pInBin);
    m_pInQueueLevel         = CreateElement("queue",            m_pInBin);
    m_pInConvertLevel       = CreateElement("audioconvert",     m_pInBin);
    m_pInLevel              = CreateElement("level",            m_pInBin);
    m_pFakeSinkLevel        = CreateElement("fakesink",         m_pInBin);

    m_pInQueueScope         = CreateElement("queue",            m_pInBin);
    m_pInConvertScope       = CreateElement("audioconvert",     m_pInBin);
    //m_pInConvertResample    = CreateElement("audioresample",    m_pInBin);
    m_pFakeSinkScope        = CreateElement("appsink",          m_pInBin);

    m_pInTeeSpectrum        = CreateElement("tee",              m_pInBin);
    m_pInQueueSpectrum      = CreateElement("queue",            m_pInBin);
    m_pInSpectrumResample   = CreateElement("audioresample",    m_pInBin);
    m_pInSpectrum           = CreateElement("spectrum",         m_pInBin);
    m_pFakeSinkSpectrum     = CreateElement("fakesink",         m_pInBin);


    caps = gst_caps_from_string(CAPS_INPUTFLOAT);
    if (!gst_element_link_filtered(m_pInSrc, m_pInVolume, caps)) {
        emit Error(QString("Cannot link filtered"));
        return false;
    }
    gst_caps_unref(caps);


    if (!gst_element_link(m_pInVolume, m_pInTeeSpectrum)) {
        emit Error(QString("Cannot create input pipeline"));
        return false;
    }

    caps = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, AUDIOFREQ, NULL);
    if (!gst_element_link(m_pInQueueSpectrum, m_pInSpectrumResample) ||
            !gst_element_link_filtered(m_pInSpectrumResample, m_pInSpectrum, caps)) {
        emit Error(QString());
        return false;
    }
    gst_caps_unref(caps);

    if (!gst_element_link_many(m_pInSpectrum, m_pFakeSinkSpectrum, NULL)) {
        emit Error(QString("Cannot create input pipeline"));
        return false;
    }

    if (!gst_element_link_many(m_pInQueueChebBand, m_pInChebBand, m_pInTee, NULL)) {
        emit Error(QString("Cannot create input pipeline"));
        return false;
    }

    if (!gst_element_link_many(m_pInQueueLevel, m_pInConvertLevel, m_pInLevel, m_pFakeSinkLevel, NULL)) {
        emit Error(QString("Cannnot link level queue"));
        return false;
    }


    if (!gst_element_link(m_pInQueueScope, m_pInConvertScope)) {
        emit Error(QString("Cannot link scoped queue"));
        return false;
    }

    caps = gst_caps_from_string(CAPS_INPUTINT);
    if (!gst_element_link_filtered(m_pInConvertScope, m_pFakeSinkScope, caps)) {
        emit Error(QString("Cannot link resample"));
        return false;
    }
    gst_caps_unref(caps);


    GstPadTemplate *tee_src_pad_template;
    tee_src_pad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(m_pInTee), "src_%u");

    GstPad *tee_pad_level, *tee_pad_scope;
    GstPad *queue_pad_level, *queue_pad_scope;

    tee_pad_level = gst_element_request_pad(m_pInTee, tee_src_pad_template, NULL, NULL);
    queue_pad_level = gst_element_get_static_pad(m_pInQueueLevel, "sink");

    tee_pad_scope = gst_element_request_pad(m_pInTee, tee_src_pad_template, NULL, NULL);
    queue_pad_scope = gst_element_get_static_pad(m_pInQueueScope, "sink");

    if (gst_pad_link(tee_pad_level, queue_pad_level) != GST_PAD_LINK_OK) {
        qDebug() << "Cannot link level pads";
        return false;
    }

    if (gst_pad_link(tee_pad_scope, queue_pad_scope) != GST_PAD_LINK_OK) {
        qDebug() << "Cannot link scope pads";
        return false;
    }
    gst_object_unref(GST_OBJECT(tee_src_pad_template));
    gst_object_unref(GST_OBJECT(tee_pad_level));
    gst_object_unref(GST_OBJECT(tee_pad_scope));
    gst_object_unref(GST_OBJECT(queue_pad_level));
    gst_object_unref(GST_OBJECT(queue_pad_scope));



    tee_src_pad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(m_pInTeeSpectrum), "src_%u");
    GstPad *tee_pad_spectrum, *tee_pad_cheb;
    GstPad *queue_pad_spectrum, *queue_pad_cheb;

    tee_pad_cheb = gst_element_request_pad(m_pInTeeSpectrum, tee_src_pad_template, NULL, NULL);
    queue_pad_cheb = gst_element_get_static_pad(m_pInQueueChebBand, "sink");
    if (gst_pad_link(tee_pad_cheb, queue_pad_cheb) != GST_PAD_LINK_OK) {
        qDebug() << "Cannot link cheb pads";
        return false;
    }

    tee_pad_spectrum = gst_element_request_pad(m_pInTeeSpectrum, tee_src_pad_template, NULL, NULL);
    queue_pad_spectrum = gst_element_get_static_pad(m_pInQueueSpectrum, "sink");
    if (gst_pad_link(tee_pad_spectrum, queue_pad_spectrum) != GST_PAD_LINK_OK) {
        qDebug() << "Cannot link spectrum pads";
        return false;
    }

    gst_object_unref(GST_OBJECT(tee_src_pad_template));
    gst_object_unref(GST_OBJECT(tee_pad_cheb));
    gst_object_unref(GST_OBJECT(tee_pad_spectrum));
    gst_object_unref(GST_OBJECT(queue_pad_cheb));
    gst_object_unref(GST_OBJECT(queue_pad_spectrum));

    //g_object_set(G_OBJECT(m_pInSrc), "freq", 32.0, NULL);
    g_object_set(G_OBJECT(m_pInLevel), "message", TRUE, "interval", GST_SECOND, NULL);
    g_object_set(G_OBJECT(m_pFakeSinkLevel), "sync", TRUE, NULL);
    g_object_set(G_OBJECT(m_pFakeSinkScope), "emit-signals", TRUE, "sync", FALSE, "blocksize", 8192, NULL);
    g_object_set(G_OBJECT(m_pFakeSinkSpectrum), "sync", TRUE, NULL);
    g_object_set(G_OBJECT(m_pInSpectrum), "bands", SPECTRAL_BANDS, "threshold", -80,
                 "post-messages", TRUE, "message-phase", FALSE, "interval", GST_SECOND, NULL);

    g_signal_connect(m_pFakeSinkScope, "new-sample",
                     G_CALLBACK(on_new_sample_sink), this);


    bus = gst_element_get_bus(m_pInPipeline);
    gst_bus_add_watch(bus, (GstBusFunc)on_in_message, this);
    gst_object_unref(bus);

    setFreq(m_dFreq);
    setBandwidth(m_dBandwidth);
    return TRUE;
}

void GstEngine::setFreq(int freq)
{
    double lower_freq, upper_freq;
    m_dFreq = freq;
    upper_freq = m_dFreq + m_dBandwidth/2.0;
    lower_freq = m_dFreq - m_dBandwidth/2.0;
    if (lower_freq < 1.0)
        lower_freq = 1.0;

    g_object_set(G_OBJECT(m_pOutSrc), "freq", m_dFreq, NULL);
    g_object_set(G_OBJECT(m_pInChebBand), "lower-frequency", lower_freq, "upper-frequency", upper_freq, NULL);
}

void GstEngine::setInGain(int gain)
{
    m_dInGain = gain / 100.0;
    g_object_set(G_OBJECT(m_pInVolume), "volume", m_dInGain, NULL);
}

void GstEngine::setOutGain(int gain)
{
    m_dOutGain = gain / 1000.0;
    g_object_set(G_OBJECT(m_pOutVolume), "volume", m_dOutGain, NULL);
}

void GstEngine::setBandwidth(int bandwidth)
{
    switch (bandwidth) {
    case 0:
        m_dBandwidth = 1.0;
        break;
    case 1:
        m_dBandwidth = 2.0;
        break;
    case 2:
        m_dBandwidth = 5.0;
        break;
    case 3:
        m_dBandwidth = 10.0;
        break;
    case 4:
        m_dBandwidth = 15.0;
        break;
    case 5:
        m_dBandwidth = 20.0;
        break;
    case 6:
        m_dBandwidth = 50.0;
        break;
    case 7:
        m_dBandwidth = 10000.0;
        break;
    default:
        m_dBandwidth = 10000.0;
        break;
    }

    double lower_freq, upper_freq;
    upper_freq = m_dFreq + m_dBandwidth/2.0;
    lower_freq = m_dFreq - m_dBandwidth/2.0;
    if (lower_freq < 1.0)
        lower_freq = 1.0;
    g_object_set(G_OBJECT(m_pInChebBand), "lower-frequency", lower_freq, "upper-frequency", upper_freq, NULL);
}

void GstEngine::setBandwidth(double bandwidth)
{
    double lower_freq, upper_freq;
    m_dBandwidth = bandwidth;
    upper_freq = m_dFreq + m_dBandwidth/2.0;
    lower_freq = m_dFreq - m_dBandwidth/2.0;
    if (lower_freq < 1.0)
        lower_freq = 1.0;

    g_object_set(G_OBJECT(m_pInChebBand), "lower-frequency", lower_freq, "upper-frequency", upper_freq, NULL);
}

bool GstEngine::changeState()
{
    if (m_bIsPlaying) {
        gst_element_set_state(m_pInPipeline, GST_STATE_PAUSED);
        gst_element_set_state(m_pOutPipeline, GST_STATE_PAUSED);
        m_bIsPlaying = false;
        emit Rms(-200.0);
    } else {
        GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(m_pInBin), GST_DEBUG_GRAPH_SHOW_ALL, "in");
        GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(m_pOutBin), GST_DEBUG_GRAPH_SHOW_ALL, "out");

        gst_element_set_state(m_pInPipeline, GST_STATE_PLAYING);
        gst_element_set_state(m_pOutPipeline, GST_STATE_PLAYING);

        m_bIsPlaying = true;
    }
    return m_bIsPlaying;
}


GstElement *GstEngine::CreateElement(const QString &factoryName, GstElement *bin)
{
    QString name = factoryName + "-" + QString::number(m_iNextElementId++);
    GstElement *element = gst_element_factory_make(factoryName.toAscii().constData(), name.toAscii().constData());
    if (!element) {
        emit Error(QString("GStreamer could not create element: %1.").arg(factoryName));
        gst_object_unref(GST_OBJECT(bin));
        return NULL;
    }

    if (bin)
        gst_bin_add(GST_BIN(bin), element);

    return element;
}

gboolean GstEngine::on_out_message(GstBus *bus, GstMessage *message, GstEngine *instance)
{
    (void) bus;
    (void) instance;
    switch (GST_MESSAGE_TYPE(message)) {
    case GST_MESSAGE_EOS:
        qDebug() << "The source got dry";
        break;
    case GST_MESSAGE_ERROR:
        qDebug() << "Error Out";
        break;
    default:
        break;
    }
    return TRUE;
}

gboolean GstEngine::on_in_message(GstBus *bus, GstMessage *message, GstEngine *instance)
{
    (void) bus;
    if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS) {
        qDebug() << "The sink got dry";
    }

    if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
        qDebug() << "Error in";
    }

    if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ELEMENT) {
        const GstStructure *s = gst_message_get_structure(message);
        const gchar *name = gst_structure_get_name(s);
        GstClockTime endtime;

        if (strcmp(name, "level") == 0) {    
            gdouble rms_dB;
            const GValue *array_val;
            GValueArray *rms_arr;

            if (!gst_structure_get_clock_time(s, "endtime", &endtime))
                endtime = GST_CLOCK_TIME_NONE;

            array_val = gst_structure_get_value(s, "rms");
            rms_arr = (GValueArray *)g_value_get_boxed(array_val);
            rms_dB = g_value_get_double(rms_arr->values + 0);
            emit instance->Rms(rms_dB);
        }

        if (strcmp(name, "spectrum") == 0) {
            const GValue *magnitudes;
            const GValue *mag;

            if (!gst_structure_get_clock_time(s, "endtime", &endtime))
                endtime = GST_CLOCK_TIME_NONE;

            //g_print("New spectrum message, endtime %" GST_TIME_FORMAT "\n", GST_TIME_ARGS(endtime));
            magnitudes = gst_structure_get_value(s, "magnitude");

            for (uint i = 0; i < SPECTRAL_BANDS; i++) {
                mag = gst_value_list_get_value(magnitudes, i);

                if (mag != NULL)
                    instance->m_magnitude.push_back(g_value_get_float(mag));
                else
                    instance->m_magnitude.push_back(-80.0);
            }
            emit instance->sendMagnitude(instance->m_magnitude);
            instance->m_magnitude.clear();
        }
    }
    return TRUE;
}

GstFlowReturn GstEngine::on_new_sample_sink(GstElement *elt, GstEngine *instance)
{
    GstSample *sample;
    GstBuffer *buffer;
    GstMapInfo map;

    sample = gst_app_sink_pull_sample(GST_APP_SINK(elt));
    buffer = gst_sample_get_buffer(sample);
    gst_sample_unref(sample);

    gst_buffer_map(buffer, &map, GST_MAP_READ);
    instance->m_iDuration += GST_BUFFER_DURATION(buffer);
    int32_t *data = (int32_t *)map.data;
    for (gsize i = 0; i < map.size/4; i++) {
        instance->m_scope.push_back(*data);
        data++;
    }

    if (instance->m_iDuration > MAX_DURATION) {
        emit instance->sendScope(instance->m_scope, instance->m_iDuration);
        instance->m_iDuration = 0;
        instance->m_scope.clear();
    }
    gst_buffer_unmap(buffer, &map);

    return GST_FLOW_OK;
}
