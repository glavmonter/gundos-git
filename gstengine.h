#ifndef GSTENGINE_H
#define GSTENGINE_H

#include <QObject>
#include <QDebug>
#include <gst/gst.h>
#include <gst/app/gstappsink.h>


#define AUDIOFREQ           800
#define SPECTRAL_BANDS      1024

#define CAPS_OUTPUT         "audio/x-raw, channels=1"
#define CAPS_INPUTFLOAT     "audio/x-raw, format=F32LE, channels=1, rate=44100"
#define CAPS_INPUTRESAMPLE  "audio/x-raw, channels=1, rate=44100"
#define CAPS_INPUTINT       "audio/x-raw, format=S32LE, channels=1"

/* 100 ms */
#define MAX_DURATION        (1000000*100)

typedef std::vector<int32_t> Scope;
typedef std::vector<double> Magnitude;

class GstEngine : public QObject
{
    Q_OBJECT
public:
    GstEngine(QObject *parent = 0);
    ~GstEngine();

    bool InitEngine();
    bool IsPlaying() {return m_bIsPlaying;}

signals:
    void Error(const QString &);
    void Rms(double);
    void sendScope(Scope &scope, quint64 duration);
    void sendMagnitude(Magnitude &magnitude);

public slots:
    void setFreq(int freq);
    void setInGain(int gain);
    void setOutGain(int gain);
    void setBandwidth(int bandwidth);
    void setBandwidth(double bandwidth);
    bool changeState();

private:
    /* Всякие параметры тракта */
    double m_dOutGain;
    double m_dInGain;
    double m_dFreq;
    double m_dBandwidth;
    bool m_bIsPlaying;
    Scope m_scope;
    qint64 m_iDuration;
    Magnitude m_magnitude;

    /* Элементы генератора */
    GstElement *m_pOutBin;
    GstElement *m_pOutPipeline;

    GstElement *m_pOutSrc;
    GstElement *m_pOutConvert;
    GstElement *m_pOutVolume;
    GstElement *m_pOutSink;


    /* Элементы входного тракта */
    GstElement *m_pInBin;
    GstElement *m_pInPipeline;

    GstElement *m_pInSrc;
    GstElement *m_pInVolume;
    GstElement *m_pInConvert;
    GstElement *m_pInQueueChebBand;
    GstElement *m_pInChebBand;

    GstElement *m_pInTeeSpectrum;
    GstElement *m_pInQueueSpectrum;
    GstElement *m_pInSpectrumResample;
    GstElement *m_pInSpectrum;
    GstElement *m_pFakeSinkSpectrum;

    GstElement *m_pInTee;
    GstElement *m_pInQueueLevel;
    GstElement *m_pInConvertLevel;
    GstElement *m_pInLevel;
    GstElement *m_pFakeSinkLevel;

    GstElement *m_pInQueueScope;
    GstElement *m_pInConvertScope;
    GstElement *m_pInConvertResample;
    GstElement *m_pFakeSinkScope;

    GstElement *CreateElement(const QString &factoryName, GstElement *bin);
    int m_iNextElementId;


    static gboolean on_out_message(GstBus *bus, GstMessage *message, GstEngine *instance);
    static gboolean on_in_message(GstBus *bus, GstMessage *message, GstEngine *instance);
    static GstFlowReturn on_new_sample_sink(GstElement *elt, GstEngine *instance);
};

#endif // GSTENGINE_H
