#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QTimer>
#include "gstengine.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updateRMS(double rms);
    void receiveScope(Scope & scope, quint64 duration);

private slots:
    void writeSettings();
    void on_btnStart_clicked();

    void on_sliderOutGain_valueChanged();
    void on_sliderOutFreq_valueChanged();
    void on_sliderInGain_valueChanged();
    void on_cmbBandwidth_currentIndexChanged();

    void on_sliderInterval_valueChanged();

private:
    Ui::MainWindow *ui;
    GstEngine *m_pEngine;

    QTimer *m_pTimer;

    Scope m_Scope;
    qint64 m_iDuration;
};

#endif // MAINWINDOW_H
