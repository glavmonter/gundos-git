#include "scopewidget.h"

ScopeWidget::ScopeWidget(QWidget *parent):
    QwtPlot(parent)
{
    setAutoReplot(false);
    //canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false);
    InitGradient();

    plotLayout()->setAlignCanvasToScales(true);
    setAxisTitle(QwtPlot::xBottom, "t, ms");
    setAxisScale(QwtPlot::xBottom, 0.0, 100.0);
    setAxisTitle(QwtPlot::yLeft, "Amplitude");
    setAxisScale(QwtPlot::yLeft, -20000000, 20000000);

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::green, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    InitData();

    m_pCurveScope = new QwtPlotCurve();
    m_pCurveScope->setStyle(QwtPlotCurve::Lines);
    m_pCurveScope->setPen(QPen(Qt::red, 2));
    m_pCurveScope->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    m_pCurveScope->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    m_pCurveScope->setRawSamples(m_ScopePosition.data(), m_Scope.data(), m_ScopePosition.size());
    m_pCurveScope->attach(this);
}

ScopeWidget::~ScopeWidget()
{
}

void ScopeWidget::receiveScope(Scope &scope, quint64 duration)
{
    (void)duration;

    /* TODO Триггер передрочть!!! */
    /* Ищем триггер в 0 */
    size_t trigger = 0;
    for (size_t i = 0; i < scope.size() - 1; i++) {
        int32_t dot, next_dot;
        dot = scope[i];
        next_dot = scope[i + 1];
        if ((dot < 0) && (next_dot > 0)) {
            trigger = i;
            break;
        }
    }

    for (size_t i = 0; i < scope.size() - trigger; i++)
        m_Scope[i] = scope[i + trigger];
    replot();
}

void ScopeWidget::setVertical(int interval)
{
    verticalInterval.setMinValue(-interval);
    verticalInterval.setMaxValue(interval);
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());
    replot();
}

void ScopeWidget::InitGradient()
{
    QPalette pal = canvas()->palette();
    QLinearGradient gradient(0.0, 0.0, 1.0, 0.0);
    gradient.setCoordinateMode(QGradient::StretchToDeviceMode);
    gradient.setColorAt(0.0, QColor(0, 49, 110));
    gradient.setColorAt(1.0, QColor(0, 87, 174));

    pal.setBrush(QPalette::Window, QBrush(gradient));
    canvas()->setPalette(pal);
}

void ScopeWidget::InitData()
{
    double dx = 100.0/4410.0;
    double x = 0.0;
    for (int i = 0; i < 4410; i++) {
        m_ScopePosition.push_back(x);
        x += dx;
        m_Scope.push_back(0.0);
    }
}

