#-------------------------------------------------
#
# Project created by QtCreator 2013-10-04T08:10:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Gundos
TEMPLATE = app
CONFIG += debug_and_release

SOURCES += main.cpp\
        mainwindow.cpp \
        gstengine.cpp \
    scopewidget.cpp \
    fftwidget.cpp

HEADERS  += mainwindow.h \
        gstengine.h \
    scopewidget.h \
    fftwidget.h

FORMS    += mainwindow.ui

unix {
    CONFIG += link_pkgconfig
    PKGCONFIG += gstreamer-1.0 gstreamer-app-1.0
}

LIBS += -lqwt
INCLUDEPATH += /usr/include/qwt6
